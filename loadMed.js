let cardCont = document.getElementById("card-body");


let allMed = () =>{
    let myToken = localStorage.getItem("Token");

    fetch("https://wazzy-drone-api.herokuapp.com/api/v1/medication",{

        headers:{
            "Authorization": `Bearer ${myToken}`,
        }

    })
        .then(res => res.json())
        .then(data => {
            let AllDronesArr = data.additionalInfo.drones;

            cardCont.innerHTML = AllDronesArr.map((x, y) => {
                return `
                 <div class="underline p-2" id = ${x.id}>
                  <p>name: ${x.name}</p>
                  <p>weight: ${x.weight}</p>
                  <p>image: ${x.image}</p>
                  <p>createdAt: ${x.createdAt}</p>

                 </div>
    `
            }).join("");
        })


};
allMed();




