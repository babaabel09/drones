let droneDetails = document.getElementById("drone-details");
let serialNo = document.getElementById("serialNo");
let weight = document.getElementById("weight");
let battery = document.getElementById("battery");
let state = document.getElementById("state");
let cardCont = document.getElementById("card-body");
let err = document.getElementById("error");
let err1 = document.getElementById("error1");
let userSerial = document.getElementById("userSerialNo");
let loadInp = document.getElementById("loadInp");
let Formbtn = document.getElementById("Formbtn");
let modalForm = document.getElementById("Form1");
let modalCardBody = document.getElementById("modal-card-body")
let userSerial1 = document.getElementById("userSerialNo1");
let loadInp1 = document.getElementById("loadInp1");
let Formbtn1 = document.getElementById("Formbtn1");
let modalForm1 = document.getElementById("Form2");
 let underline = document.getElementsByClassName("underline");




let drones = JSON.parse(localStorage.getItem("drones")) || [];
let loadMedDrone = JSON.parse(localStorage.getItem("loadMed")) ||[];


let myToken = localStorage.getItem("Token");

let render = () =>{
    cardCont.innerHTML = drones.map((x, y) => {
        return `
     <div class="underline p-2" id = ${y}>
      serialNo:<p>${x.serialNo}</p>
      weightLimit:<p>${x.weightLimit}</p>
      BatteryCapacity:<p>${x.batteryCapacity}</p>
      State:<p class="state"> ${x.state}</p>
     <button id="loadMedBtn" class="loadMedBtn" onclick="loadMed(this)" data-bs-toggle="modal" data-bs-target="#Form1">load Medication</button>
     <button onclick="loadedMed(this)" data-bs-toggle="modal" data-bs-target="#Form2">loaded Medication</button>
     <button onclick="viewBar(this)">Battery Info</button>

     </div>
    `
    }).join("");

};
render();





droneDetails.addEventListener('submit', (e) =>{
    e.preventDefault();
    let serial = serialNo.value;
    let weightOfDrone = weight.value;
    let batteryCapacity = battery.value;
    let stateOfDrone = state.value;

    switch ((weightOfDrone < 50 || weightOfDrone > 500) || (batteryCapacity < 0 || batteryCapacity > 100)) {
        case (weightOfDrone < 50 || weightOfDrone > 500) || (batteryCapacity < 0 || batteryCapacity > 100):
            if(weightOfDrone < 50 || weightOfDrone > 500){
                 err.classList.remove("error");
            } else{
                err.classList.add("error");

            }

            if(batteryCapacity < 0 || batteryCapacity > 100){
                err1.classList.remove("error");

            } else{
                err1.classList.add("error");

            }

            if(!((weightOfDrone < 50 || weightOfDrone > 500) || (batteryCapacity < 0 || batteryCapacity > 100))) {

                let myToken = localStorage.getItem("Token");
                const body = {
                            drones:[]

                };
                let droneArr = body.drones;
                let bodyArr = {
                    serialNo: serial,
                    weightLimit: weightOfDrone,
                    batteryCapacity: batteryCapacity,
                    state: stateOfDrone
                };
                 droneArr.push(bodyArr);
                 drones.push(bodyArr)

                fetch("https://wazzy-drone-api.herokuapp.com/api/v1/drone", {

                    method: "POST",
                    headers: {
                        "Authorization": `Bearer ${myToken}`,
                        "content-type": "application/json",
                    },
                    body: JSON.stringify(body)
                })
                    .then(res => res.json())
                    .then(data => {

                        if (data.status !== "201 CREATED"){
                            console.log(data);
                        }else{
                            console.log(data);
                            render();
                            localStorage.setItem("drones", JSON.stringify(drones));
                            location.reload();
                        }
                    })
                    .catch(err => console.log(err))
                droneDetails.reset();

            }
          break;
    }

});

let stateOf = document.querySelectorAll(".state");
let stateArr = Array.from(stateOf);
stateArr.forEach(function (arr) {
    if (arr.innerText === "IDLE"){
        let hide = arr.nextElementSibling;
        hide.classList.remove("rmv-show")
    }else{
        let hide = arr.nextElementSibling;
        hide.classList.add("rmv-show")
    }
});


function addMed(e) {
    let selectedItem = e.parentElement.id;
    let search = drones.find((x, y) => y == selectedItem  )
    let Item = search.serialNo;
    localStorage.setItem("serialNo", JSON.stringify(Item));

};


let viewBar = (e) =>{
    let serialNo = e.parentElement.firstElementChild.innerHTML;
    let myToken = localStorage.getItem("Token");

    fetch(`https://wazzy-drone-api.herokuapp.com/api/v1/drone/${serialNo}/batteryInfo`,{
        method: "GET",
        headers: {
            "Authorization": `Bearer ${myToken}`,
        },
    })
        .then(res => res.json())
        .then(data => {
            alert("battery level is" + " " + data.additionalInfo.battery_level+ "%")
        })

        .catch(err => console.log(err))

};


let loadMed = (e) =>{
    let serialNo = e.parentElement.firstElementChild.innerHTML;
    localStorage.setItem("serialNo", JSON.stringify(serialNo));

    let serial = JSON.parse(localStorage.getItem("serialNo"))
    console.log(serial)

    userSerial.innerHTML = JSON.parse(localStorage.getItem("serialNo"))


    modalForm.addEventListener('submit', (e) => {
        e.preventDefault()


        let body = {
            medicationCodes:[],
        }
        let bodyMedArr = body.medicationCodes;
        let codes = loadInp.value;
        console.log(codes)
        bodyMedArr.push(codes)
        loadMedDrone .push(codes)
        console.log(bodyMedArr)

        fetch(`https://wazzy-drone-api.herokuapp.com/api/v1/drone/${serialNo}/medication`, {

            method: "PUT",
            headers: {
                "Authorization": `Bearer ${myToken}`,
                "content-type": "application/json",
            },
            body: JSON.stringify(body)
        })
            .then(res => res.json())
            .then(data => {
                if (data.status !== "200 OK"){
                    alert("medicationCode(S)" + " "+data.status)

                }else {
                    alert(data.message)
                    localStorage.setItem("loadMed", JSON.stringify(loadMedDrone))
                    let search = drones.find((x, y) => x.serialNo === serial)
                    search.state = "LOADED";
                    localStorage.setItem("drones", JSON.stringify(drones));
                    render();
                    location.reload()
                }
            })
            .catch(err => console.log(err))

        modalForm.reset();

    })


    // render();
}

let loadedMed = (e) =>{
    let serialNo = e.parentElement.firstElementChild.innerHTML;
    localStorage.setItem("serialNo", JSON.stringify(serialNo));

    let serial = JSON.parse(localStorage.getItem("serialNo"))
    console.log(serial)

    userSerial1.innerHTML = JSON.parse(localStorage.getItem("serialNo"))

        fetch(`https://wazzy-drone-api.herokuapp.com/api/v1/drone/${serialNo}/medication`, {

            headers: {
                "Authorization": `Bearer ${myToken}`,
            },
        })
            .then(res => res.json())
            .then(data => {
                if (data.status !== "200 OK" || data.status  ===  "NOT_FOUND"){

                    let errorMsg = data.message;
                    let renderModal1 = () =>{
                        modalCardBody.innerHTML = `
       
                               <div class="underline p-2" >
                              <h3>${errorMsg}</h3>     
                               </div>
                            
                            `
                    };
                    renderModal1();
                }else {
                    let loadedDetails = data.additionalInfo.drones[0];


                    let renderModal = () =>{
                        modalCardBody.innerHTML = `
       
                               <div class="underline p-2" >
                              <p>Code: ${loadedDetails.code}</p>
                              <p>Name: ${loadedDetails.name}</p>
                              <p>Weight: ${loadedDetails.weight}</p>
                              <p>Image: ${loadedDetails.image}</p>
                            </div>
                            
                            `
                    };
                    renderModal();
                    let loadedDet = {
                        code: loadedDetails.code,
                        name: loadedDetails.name,
                        weight: loadedDetails.weight,
                    }

                     renderModal();
                }
            })
            .catch(err => console.log(err))

        modalForm.reset();




    // render();
}


let clearStore = () =>{
    localStorage.clear();
    location.href = "index.html";
}

setTimeout(() =>{
    localStorage.clear();
    alert("session deleted")
    location.href = "index.html";

}, 900000)
