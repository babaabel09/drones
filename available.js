let cardCont = document.getElementById("card-body");

//
// let render = () =>{
//
//
// };
// render();



let availableDrones = () =>{
    let myToken = localStorage.getItem("Token");

    fetch("https://wazzy-drone-api.herokuapp.com/api/v1/drone/available",{

      headers:{
          "Authorization": `Bearer ${myToken}`,
      }

})
        .then(res => res.json())
        .then(data => {
            let idleDronesArr = data.additionalInfo.drones;
            cardCont.innerHTML = idleDronesArr.map((x, y) => {
                return `
                 <div class="underline p-2" id = ${x.id}>
                  serialNo:<p>${x.serialNo}</p>
                  model:<p>${x.model}</p>
                  BatteryCapacity:<p>${x.batteryCapacity}</p>
                  createdAt:<p> ${x.createdAt}</p>
                
                 </div>
    `
            }).join("");
        })


};
availableDrones();