let cardCont = document.getElementById("card-body");


let allDroneArr = [];

const render = (allArr) =>{

        cardCont.innerHTML = allArr.map((x, y) => {
            return `
                 <div class="underline p-2 rmv-show" id = ${x.id}>
                  serialNo:<p>${x.serialNo}</p>
                  model:<p class="model">${x.model}</p>
                  state:<p class="state">${x.state}</p>
                  weightLimit:<p>${x.weightLimit}</p>
                  BatteryCapacity:<p>${x.batteryCapacity}</p>
                  createdAt:<p> ${x.createdAt}</p>
                
                 </div>
    `
        }).join("");
    formReset();

}

let allDrones = () =>{
    let myToken = localStorage.getItem("Token");

    fetch("https://wazzy-drone-api.herokuapp.com/api/v1/drone",{

        headers:{
            "Authorization": `Bearer ${myToken}`,
        }

    })
        .then(res => res.json())
        .then(data => {
            render(data.additionalInfo.drones)
            allDroneArr.push(data.additionalInfo.drones)

        });


};
allDrones();


function sortState(e) {
    let und = document.querySelectorAll(".underline");
    let undArr = Array.from(und);
    undArr.forEach(function (line) {
        let state = line.children.item(2).innerHTML;
        let rmvState = line.children.item(2).classList.contains("state");
       switch (e.value) {
           case "ALL":
               if (e.value === "ALL") {
                   line.classList.remove("rmv-sort")
               }
               break;
           case "IDLE":
               if(e.value === state && state === "IDLE"){
                  line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "LOADING":
               if(e.value === state && state === "LOADING"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "LOADED":
               if(e.value === state && state === "LOADED"){
                   line.classList.remove("rmv-sort");

               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "DELIVERING":
               if(e.value === state && state === "DELIVERING"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "DELIVERED":
               if(e.value === state && state === "DELIVERED"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }

               break;
           case "RETURNING":
               if(e.value === state && state === "RETURNING"){
                   line.classList.remove("rmv-sort");
               }else{
                   line.classList.add("rmv-sort");
               }
               break;

       }

    })

};

function sortModel(e) {
    let und = document.querySelectorAll(".underline");
    let undArr = Array.from(und);
    undArr.forEach(function (line) {
        let model = line.children.item(1).innerHTML;
        let rmvModel = line.children.item(1).classList.contains("model");
        switch (e.value) {
            case "ALL":
                if (e.value === "ALL") {

                    line.classList.remove("rmv-show");

                }
                break;
            case "LIGHTWEIGHT":
                if(e.value === model  && model === "LIGHTWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "MIDDLEWEIGHT":
                if(e.value === model && model === "MIDDLEWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "CRUISERWEIGHT":
                if(e.value === model && model === "CRUISERWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "HEAVYWEIGHT":
                if(e.value === model && model === "HEAVYWEIGHT"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }

                break;
            case "DELIVERED":
                if(e.value === model && model === "DELIVERED"){
                    line.classList.remove("rmv-show");
                }else{
                    line.classList.add("rmv-show");
                }
                break;

        }

    })



};

function formReset() {
    let sortModel = document.getElementById("sortModel");
    let sortState = document.getElementById("sortState");
    let und = document.querySelectorAll(".underline");
    let undArr = Array.from(und);
    undArr.forEach(function (line) {
        if (sortState.value === "ALL" && sortModel.value === "ALL") {
            line.classList.remove("rmv-show");
        } else {
            line.classList.add("rmv-show");

        }
    })
}


